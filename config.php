<?php

// Icinga parameters you should change
$icinga_host = "your.icinga2.host.org"
$icinga_port = 5665     // change if using non-standard port
$icinga_user = "username";
$icinga_pass = "password";

$icinga_api_path = "/v1/objects/hosts"
$icinga_url = "https://" . $icinga_host . ":" . $icinga_port . $icinga_api_path;

// modify hostgroups in this list to include those you want to display
$icinga_groups = [
    "group1",
    "group2",
    "group3"
];
// mysql database connection info
$mysql_host = "your.mysql.host.org";
$mysql_user = "mysqlusername";
$mysql_pass = "mysqlpassword";
$mysql_db = "mysqldb";

// used by WOL function
$socket = 7;
?>
