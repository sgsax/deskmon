<?php

    require_once "config.php";
    require_once "handler.php";

    $hoststates = getHostStates(getJSON());
    $cnames = getCNames(getData("cnames"));
    $hosts = getHosts(getData("hosts"), $hoststates, $cnames);
    $hostgroups = getHostGroups(getData("groups"));

    $wakelist=NULL;

    if (!empty($_GET)) {
		if (isset($_GET["host"])) {
        	$wakelist=json_decode(htmlspecialchars($_GET["host"]));
		}
    }

    $result = wakeHosts($hosts, $wakelist, $socket);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Desktop Status - WOL</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/deskmon.css" />
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <?php include "nav.php"; ?>

    <div class="container">
        <div class="row">
            <div class="span12">
                <p class="lead">WakeOnLan Result</p>
                <?php
                    foreach($wakelist as $host) {
                        print "<p>Magic packet sent to " . $hosts[$host]["name"] . " - " . $hosts[$host]["ip"] . ", " . $hosts[$host]["mac"] . " : ";
                        if ($result[$host]) {
                            print "Succeeded";
                        } else {
                            print "Failed";
                        }
                        print "</p>\n";
                    };
                ?>
            </div><!-- /.span12 -->
        </div><!-- /.row -->

    </div><!-- /.container -->

</body>
</html>

