<?php

    require_once "config.php";
    require_once "handler.php";

    $hoststates = getHostStates(getJSON());
    $cnames = getCNames(getData("cnames"));
    $hosts = getHosts(getData("hosts"), $hoststates, $cnames);
    $hostgroups = getHostGroups(getData("groups"));

    $hostgroup=NULL;

    if (!empty($_GET)) {
		if (isset($_GET["hostgroup"])) {
        	$hostgroup=htmlspecialchars($_GET["hostgroup"]);
		}
    }

?>

<!DOCTYPE html>
<html>
<head>
    <title>Desktop Status</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/deskmon.css" />
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <?php include "nav.php"; ?>

    <div class="container">
        <div class="row collapse <?php if ($hostgroup==NULL) { print "in"; } ?>">
            <div class="span12">
                <a href="wake.php?host=<?php print json_encode(getHostIDs($hosts)); ?>" role="button" class="btn btn-primary">Wake all hosts</a>
            </div><!-- /.span12 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="span12">
                <?php printHosts($hosts, $hostgroups, $hostgroup); ?>
            </div><!-- /.span12 -->
        </div><!-- /.row -->

    </div><!-- /.container -->

</body>
</html>

