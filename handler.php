<?php

function getJSON() {
    require "config.php";

    $headers = array(
        'Accept: application/json',
        'X-HTTP-Method-Override: GET'
    );

    $filter = "";
    foreach ($icinga_groups as $group) {
        $filter = $filter . '("' . $group .'" in host.groups)||';
    }
    $filter = substr($filter, 0 , -2);

    $data = array(
        'attrs' => array('name', 'state'),
        'filter' => $filter,
    );

    $hosts = array();

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $icinga_url,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_USERPWD => $icinga_user . ":" . $icinga_pass,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_HTTPAUTH => true,
        CURLAUTH_BASIC => true,
        CURLOPT_POST => count($data),
        CURLOPT_POSTFIELDS => json_encode($data)
    ));

    $response = curl_exec($ch);
    if ($response === false) {
        print "Error: " . curl_error($ch) . "(" . $response . ")\n";
    }

    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($code == 200) {
        return json_decode($response, true);
    } else {
        return array("Error retrieving data");
    }
}

function getData($table) {
    require "config.php";

    $ret = array();
    $conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,$mysql_db);

    if ($conn === false) {
        array_push($ret, mysqli_connect_error());
    } else {
        $sql = "select * from $table";
        $result = mysqli_query($conn,$sql);
        if ($result === false) {
            array_push($ret, "No data for $table");
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($ret, $row);
            }
        }
    }

    return $ret;
}

function getHostStates($hoststates) {
    $ret = array();

    foreach ($hoststates["results"] as $state) {
        $ret[$state["attrs"]["name"]] = $state["attrs"]["state"];
    }

	return $ret;
}

function getHosts($rawhosts, $hoststates, $cnames) {
    $ret = array();

    foreach ($rawhosts as $rawhost) {
		if (array_key_exists($rawhost["id"], $cnames)) {
			$cn = $cnames[$rawhost["id"]];
		} else {
			$cn = array();
		}

        $ret[$rawhost["id"]] = array(
            "id" => $rawhost["id"],
            "name" => $rawhost["host_name"],
            "mac" => $rawhost["mac"],
            "ip" => $rawhost["ip"],
            "group_id" => $rawhost["group_id"],
            "state" => $hoststates[$rawhost["host_name"]],
            "cnames" => $cn
        );
    }

	return $ret;
}

function getHostGroups($rawgroups) {
	$ret = array();

    foreach ($rawgroups as $rawgroup) {
        $ret[$rawgroup["id"]] = $rawgroup["group_name"];
    }

	return $ret;
}

function getCNames($rawcnames) {
	$ret = array();

    foreach ($rawcnames as $rawcname) {
        if (!array_key_exists($rawcname["host_id"], $ret)) {
            $ret[$rawcname["host_id"]] = array();
        }
        array_push($ret[$rawcname["host_id"]], $rawcname["cname"]);
    }

	return $ret;
}

function getState($state) {
	if ($state == 0) {
		return "success";
	} elseif ($state ==1) {
		return "danger";
	} else {
		return "warning";
	}
}

function getHostsFromGroup($hosts, $hostgroup) {
    $ret = array();

    $subset = array_filter($hosts,
        function ($h) use ($hostgroup) {
            return ($h["group_id"] == $hostgroup);
        }
    );
    
    foreach ($subset as $host_id => $host) {
        array_push($ret, $host_id);
    }

    return json_encode($ret);
}

function getHostIDs($hosts) {
    $ret = array();

    foreach ($hosts as $host_id => $host) {
        array_push($ret, $host_id);
    }

    return $ret;
}

function sortHostnames($a, $b){
    return strnatcmp($a["name"], $b["name"]);
}

function printCnames($host) {
	if (count($host["cnames"]) > 0) {
		return "(" . implode(",", $host["cnames"]) . ")";
	} else {
		return "";
	}
}

function printGroup($hosts) {
	$i=1;

    usort($hosts, "sortHostnames");

    print "<div class=\"row\">\n";
    foreach($hosts as $host){
        print "    <div class=\"col-lg-2\">\n";
        print "        <div class=\"alert alert-" . getState($host["state"]) . "\">\n";
        print "            <a href=\"wake.php?host=" . json_encode(array($host["id"]), JSON_NUMERIC_CHECK) . "\" class=\"alert-link\"><strong>" . $host["name"] . "</strong> " . printCnames($host) . "</a><br>\n";
		print "            IP: $host[ip]<br>\n";
        print "            MAC: $host[mac]<br>\n";
        print "        </div><!-- alert -->\n";
        print "    </div><!-- col -->\n";
        if(($i%6) == 0) {
            print "</div><!-- row -->\n";
            print "<div class=\"clearfix visible-lg-block\"></div>\n";
            print "<div class=\"row\">\n";
            $i=1;
        } else {
            $i+=1;
        }
    };
    print "</div><!-- row -->\n";

}

function printHosts($hosts, $hostgroups, $hostgroup) {
    $displaygroups = array();

    if (isset($hostgroup)) {
        $displaygroups[$hostgroup] = $hostgroups[$hostgroup];
    } else {
        $displaygroups = $hostgroups;
    }

    foreach ($displaygroups as $group_id => $groupname) { 
        print "<h2>$groupname</h2>\n";
        print "<a href=\"wake.php?host=" . getHostsFromGroup($hosts, $group_id) . "\" role=\"button\" class=\"btn btn-primary\">Wake all in this group</a>\n";
        print "<p>\n";
        printGroup(array_filter($hosts,
            function ($h) use ($group_id) {
                return ($h["group_id"] == $group_id);
            }
        ));
    }
}

function WakeOnLan($addr, $mac, $socket_number) {
/* borrowed from http://www.hackernotcracker.com/2006-04/wol-wake-on-lan-tutorial-with-bonus-php-script.html
   Wake on LAN - (c) HotKey@spr.at, upgraded by Murzik
   Modified by Allan Barizo http://www.hackernotcracker.com
*/

    $addr_byte = explode(':', $mac);
    $hw_addr = '';
    for ($a=0; $a <6; $a++) $hw_addr .= chr(hexdec($addr_byte[$a]));
    $msg = chr(255).chr(255).chr(255).chr(255).chr(255).chr(255);
    for ($a = 1; $a <= 16; $a++) $msg .= $hw_addr;
    // send it to the broadcast address using UDP
    // SQL_BROADCAST option isn't help!!
    $s = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    if ($s == false) {
        error_log("Error creating socket!");
        error_log("Error code is '".socket_last_error($s)."' - " . socket_strerror(socket_last_error($s)));
        return FALSE;
    } else {
        // setting a broadcast option to socket:
        $opt_ret = socket_set_option($s, 1, 6, TRUE);
        if($opt_ret <0) {
            error_log("setsockopt() failed, error: " . strerror($opt_ret));
            return FALSE;
        }
        if(socket_sendto($s, $msg, strlen($msg), 0, $addr, $socket_number)) {
            error_log("Magic Packet sent successfully!");
            socket_close($s);
            return TRUE;
        }
        else {
            error_log("Magic packet failed!");
            return FALSE;
        }
     
    }
}

function wakeHosts($hosts, $wakelist, $socket) {
    $ret = array();

    foreach ($wakelist as $host) {
        $result = WakeOnLan($hosts[$host]["ip"], $hosts[$host]["mac"], $socket);
        $ret[$host] = $result;
    }

    return $ret;
}

?>
