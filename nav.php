<?php 
/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * nav.php
 *
 * prepare and display navagation header used for all pages
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */


require_once "config.php";
require_once "handler.php";

function echoActiveClassIfRequestMatches($requestUri) {
/* highlight menu item for current page */
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri) {
        echo 'class="active"';
    }
}

function buildHostgroupMenu() {
/* get list of all hostgroups and generate list of links to show data for */
/*   each of them; used in drop-down menu item */
    $hostgrps = getData("groups");

    foreach($hostgrps as $hostgrp) {
        print "<li><a href=\"/index.php?hostgroup=$hostgrp[id]\">$hostgrp[group_name]</a></li>\n";
    }
}
?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Desktop Status</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li <?=echoActiveClassIfRequestMatches("")?>><a href="/">All Hosts</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hostgroups <span class="caret"></span></a>
                    <ul class="dropdown-menu scrollable-menu" role="menu">
                        <li><a href="/">All</a></li>
                        <li class="divider"></li>
                        <?php buildHostgroupMenu() ?>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
