To use this dashboard, you must create a database to hold info on host IP and MAC addresses. Import the included `deskmon.sql` schema into your database to create the tables, and then populate them accordingly.

Loading this data is an exercise left to the reader, but the schema should be enough info to get you started. The `groups` table does not necessarily need to use group names in your icinga data, but something readable for display purposes.

You will need to modify `config.php` to work with your installation of Icinga, set the list of hostgroups you want to display, and your database connection parameters.
